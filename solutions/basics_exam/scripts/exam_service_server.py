#! /usr/bin/env python

import rospy
from std_srvs.srv import Trigger, TriggerResponse
from sensor_msgs.msg import LaserScan
import time


class CrashDirectionService(object):
    def __init__(self, srv_name='/crash_direction_service'):
        self._srv_name = srv_name
        self.detection_dict = {"front":0.0, "all_left":0.0, "all_right":0.0, "left":0.0, "right":0.0}
        self._my_service = rospy.Service(self._srv_name, Trigger , self.srv_callback)
        self._laser_sub = rospy.Subscriber('/scan', LaserScan, self.topic_callback)
        self._laserdata = LaserScan()

    def srv_callback(self, request):
        self.crash_detector()
        
        message = self.direction_to_move()
        
        rospy.logdebug("DIRECTION ==> "+message)
        
        response = TriggerResponse()
        """
        ---                                                                                                 
        bool success   # indicate if crashed                                       
        string message # Direction
        """
        response.success = True
        response.message = message
        
        return response
        
    def topic_callback(self, msg):
        self._laserdata = msg
        
    def crash_detector(self):
        
        self._front = self._laserdata.ranges[360]
        self._left = self._laserdata.ranges[0]
        self._right = self._laserdata.ranges[719]
        self._left2 = self._laserdata.ranges[300]
        self._right2 = self._laserdata.ranges[420]
        rospy.loginfo("Front Distance == "+str(self._front))
        rospy.loginfo("Left Distance == "+str(self._left))
        rospy.loginfo("Right Distance == "+str(self._right))
        rospy.loginfo("Left2 Distance == "+str(self._left2))
        rospy.loginfo("Right2 Distance == "+str(self._right2))
        
        self.detection_dict = {"front":self._front,
                          "all_left":self._left,
                          "all_right":self._right,
                          "left":self._left2,
                          "right":self._right2}
        
    
    def direction_to_move(self):

        if self.detection_dict["front"] > self.detection_dict["all_left"] and self.detection_dict["front"] > self.detection_dict["all_right"]:
            message = "front"
        
        elif self.detection_dict["all_right"] > self.detection_dict["all_left"]:
            message = "right"
        
        elif self.detection_dict["all_left"] > self.detection_dict["all_right"]:
            message = "left"
        
        if self.detection_dict["left"] < 0.7 :
            message = "right"
            
        elif self.detection_dict["right"] < 0.7 :
            message = "left"

        
        return message

if __name__ == "__main__":
    rospy.init_node('crash_direction_service_server', log_level=rospy.INFO) 
    dir_serv_object = CrashDirectionService()
    rospy.spin() # mantain the service open.